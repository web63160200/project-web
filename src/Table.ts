export default interface Table {
  id: number;
  table: string;
  people: string;
}
