export default interface Food {
  id: number;
  name: string;
  price: number;
  catagory: string;
  img: string;
  qty: number;
}
