export default interface QueServe {
  id: number;
  menu: String;
  table: number;
  status: string;
}
