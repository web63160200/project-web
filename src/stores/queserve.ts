import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type QueServe from "@/QueServe";
import type QueCook from "@/QueCook";
import { useCookStore } from "@/stores/quecook";

const queStore = useCookStore();

export const useServeStore = defineStore("queserve", () => {
  const servefinish = ref<QueServe[]>([]);
  const quefinish = ref<QueCook[]>(queStore.quefinish);

  const served = (id: number): void => {
    const index = quefinish.value.findIndex((item) => item.id === id);
    servefinish.value.push(quefinish.value[index]);
    quefinish.value.splice(index, 1);
  };

  return {
    servefinish,
    quefinish,
    served,
  };
});
