import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Table from "@/Table";

export const useTableZone = defineStore("table", () => {
  const dialog = ref(false);
  const btnOpen = ref(true);

  const zone1 = ref<Table[]>([
    { id: 1, table: "โต๊ะ 1", people: "2" },
    { id: 2, table: "โต๊ะ 2", people: "4" },
    { id: 3, table: "โต๊ะ 3", people: "6" },
    { id: 4, table: "โต๊ะ 4", people: "8" },
  ]);

  const zone2 = ref<Table[]>([
    { id: 1, table: "โต๊ะ 5", people: "2" },
    { id: 2, table: "โต๊ะ 6", people: "4" },
    { id: 3, table: "โต๊ะ 7", people: "6" },
    { id: 4, table: "โต๊ะ 8", people: "8" },
  ]);

  const qrCodeZone1 = (id: number): void => {
    const index = zone1.value.findIndex((item) => item.id === id);
    dialog.value = true;
  };

  return { zone1, zone2, dialog, qrCodeZone1 };
});
