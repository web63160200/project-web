import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMassageStore = defineStore("massage", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeout = ref(2000);
  const showMessage = (msg: string, tout: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeout.value = tout;
  };
  const closeMessage = (msg: string) => {
    message.value = "";
    isShow.value = false;
  };

  return { showMessage, closeMessage, isShow, message, timeout };
});
