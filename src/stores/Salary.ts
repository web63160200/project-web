export default interface Salary {
  id: number;
  idEmp: number;
  name: string;
  work: string;
  date: string;
  inWork: string;
  outWork: string;
  countTime: number;
  salary: number;
}
