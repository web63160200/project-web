import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type QueCook from "@/QueCook";

export const useCookStore = defineStore("quecook", () => {
  const quecook = ref<QueCook[]>([]);
  const quefinish = ref<QueCook[]>([]);
  const quewait = ref<QueCook[]>([
    { id: 1, menu: "กะเพราหมูสับ", table: 4, status: "รอเตรียม" },
    { id: 2, menu: "กะเพราหมูสับ", table: 4, status: "รอเตรียม" },
    { id: 3, menu: "ข้าวผัดหมู", table: 4, status: "รอเตรียม" },
    { id: 4, menu: "สุกี้", table: 4, status: "รอเตรียม" },
  ]);

  const deleteQue = (id: number): void => {
    const index = quewait.value.findIndex((item) => item.id === id);
    quewait.value.splice(index, 1);
  };
  const cookingQue = (id: number): void => {
    const index = quewait.value.findIndex((item) => item.id === id);
    quecook.value.push(quewait.value[index]);
    quewait.value.splice(index, 1);
  };

  const finishQue = (id: number): void => {
    const index = quecook.value.findIndex((item) => item.id === id);
    quefinish.value.push(quecook.value[index]);
    quecook.value.splice(index, 1);
  };

  return {
    quecook,
    quewait,
    quefinish,
    deleteQue,
    cookingQue,
    finishQue,
  };
});
