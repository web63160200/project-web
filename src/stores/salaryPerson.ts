import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Salary from "./Salary";

export const useSalary = defineStore("salary", () => {
  const lastId = 8;
  const mySalary = ref<Salary[]>([]);
  const person = ref<Salary[]>([

    {
        id: 1,
        idEmp: 1,
        name: "ธิดารัตน์",
        work: "ผู้จัดการร้าน",
        date: "14/02/2022",
        inWork: "09.00.00",
        outWork: "19.00.00",
        countTime: 10,
        salary: 600,
    },
    {
        id: 2,
        idEmp: 2,
        name: "องอาจ",
        work: "เชฟ",
        date: "14/02/2022",
        inWork: "10.00.00",
        outWork: "19.00.00",
        countTime: 9,
        salary: 540,
    },

    {
        id: 3,
        idEmp: 3,
        name: "สพลดนัย",
        work: "เชฟ",
        date: "14/02/2022",
        inWork: "10.00.00",
        outWork: "18.00.00",
        countTime: 8,
        salary: 480,
    }
    
  ]);

  const addSalary = (id: number): void => {
    const index = person.value.findIndex((item) => item.id === id);
    mySalary.value.push(person.value[index]);
  };

  const deleteSalary = (id: number): void => {
    const index = mySalary.value.findIndex((item) => item.id === id);
    mySalary.value.splice(index, 1);
  };

  return {
    person,
    lastId,
    mySalary,
    addSalary,
    deleteSalary,
  };
});
