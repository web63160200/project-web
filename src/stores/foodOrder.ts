import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type Food from "@/Food";

export const useFoodStore = defineStore("food", () => {
  const lastId = 8;
  const myOrder = ref<Food[]>([]);
  const menu = ref<Food[]>([
    {
      id: 1,
      name: "ข้าวผัด",
      price: 50,
      catagory: "1",
      img: "../../../public/01.png",
      qty: 1,
    },
    {
      id: 2,
      name: "ผัดไท",
      price: 50,
      catagory: "1",
      img: "../../../public/02.png",
      qty: 1,
    },
    {
      id: 3,
      name: "ต้มยำกุ้ง",
      price: 45,
      catagory: "1",
      img: "../../../public/03.png",
      qty: 1,
    },
    {
      id: 4,
      name: "ข้าวกระเพราหมูสับ",
      price: 45,
      catagory: "1",
      img: "../../../public/04.png",
      qty: 1,
    },
    {
      id: 5,
      name: "ข้าวผัดต้มยำหมู",
      price: 50,
      catagory: "1",
      img: "../../../public/05.png",
      qty: 1,
    },
    {
      id: 6,
      name: "ข้าวหมูทอดกระเทียม",
      price: 50,
      catagory: "1",
      img: "../../../public/06.png",
      qty: 1,
    },
    {
      id: 7,
      name: "ผัดซีอิ๊วหมู",
      price: 50,
      catagory: "1",
      img: "../../../public/07.png",
      qty: 1,
    },
  ]);

  const addOrder = (id: number): void => {
    const index = menu.value.findIndex((item) => item.id === id);
    myOrder.value.push(menu.value[index]);
  };

  const deleteOrder = (id: number): void => {
    const index = myOrder.value.findIndex((item) => item.id === id);
    myOrder.value.splice(index, 1);
  };

  return {
    menu,
    lastId,
    myOrder,
    addOrder,
    deleteOrder,
  };
});
